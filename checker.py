import os
import json
import glob
import hashlib


with open('dict.json') as f:
    dicts = json.load(f)

directory = 'Matlab/'
for filename in os.listdir(directory):
    file = glob.glob(directory + filename)

    newf = str(file)
    newf2 =  newf.strip( '[]' )
    newf3 = newf2.strip("''")

    with open(newf3,'rb') as getmd5:
        data = getmd5.read()
        hashh = hashlib.md5(data).hexdigest()
        # print(gethash)
        if hashh != dicts[filename]:
            print("Error in file: " + filename)
        else:
            print("OK")
