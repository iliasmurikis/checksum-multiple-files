# Checksum Multiple FIles
## Introduction 
This project has been created to check quickly the checksum of multiple files for a Matlab installation, but can be implemented to any other case by feeding any other HTML form and change the look-up format in the script.

## Checksum acquisition from webpage form with scrapping
All the checksum values were in the website so I scrapped the webpage. The Python script runs on the HTML file and looks for specific values in a specific table. It extracts the required values and stores them to a dictionary. Finally, it extracts the dictionary to a JSON file.

## Validate downloads 
The second Python file manages to read the checksum values from the JSON file and compare the truth values with the checksum of each downloaded file, for all the files in a specified folder.
