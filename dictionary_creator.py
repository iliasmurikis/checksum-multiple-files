import json
from bs4 import BeautifulSoup


with open('Select Products.html', 'r') as f:
    contents = f.read()
    soup = BeautifulSoup(contents, 'lxml')

dicts = {}
first_it_flag = True
second_it_flag = True
third_it_flag = True
for table in soup.find_all('table', attrs={"class": "table datatable"}):
    for tbody in table.find_all('tbody'):
        # print(tbody.getText().strip())
        for td in tbody.find_all('td'):
            # To ignore two first iterations
            if first_it_flag or second_it_flag or third_it_flag:
                if first_it_flag:
                    first_it_flag = False
                    continue
                elif second_it_flag:
                    second_it_flag = False
                    continue
                elif third_it_flag:
                    third_it_flag = False
                    continue

            entire = td.getText().split('\n')

            # If the only list item is small or null ignore
            if len(entire) > 0:
                if len(entire[0]) >= 25:
                    if '.zip' in entire[0]:
                        filename, hashh = entire[0].split('.zip')
                        # print(filename + '.zip', hashh)
                        dicts[filename + '.zip'] = hashh
                    elif '.exe' in entire[0]:
                        filename, hashh = entire[0].split('.exe')
                        # print(filename + '.exe', hashh)
                        dicts[filename + '.exe'] = hashh

# print(dicts)
json = json.dumps(dicts)
f = open("dict.json","w")
f.write(json)
f.close()
